# Embellish


0.2.0
=====

* Change oembed embelliser source attribute to match CKEDitor5's media embed
  output.

0.1.1
=====

* First published release