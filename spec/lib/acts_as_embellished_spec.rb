require 'rails_helper'

class Thing
  include Embellish::ActsAsEmbellished

  embellish_field :body
  embellish_field :excerpt, embellishments: []

  def excerpt
    "<custom>Lorem</custom>"
  end

  def body
    "<custom></custom><img class='custom'></img><custom>Ipsum</custom>"
  end
end

RSpec.describe Embellish::ActsAsEmbellished do
  let(:subject) { Thing.new }
  describe '::embellish_field' do
    it 'adds the field to an class attribute' do
      expect(Thing.embellished_fields.keys).to match_array(%i[body excerpt])
    end

    it 'defines a embellished_{field} method' do
      expect(subject).to respond_to(:embellished_body)
      expect(subject).to respond_to(:embellished_excerpt)
    end
  end

  describe "embellished field instance methods" do
    it 'runs configured embellishments on the field' do
      expect(Embellish::Embellisher).to receive(:new).with(subject.excerpt, [])
                                                     .and_call_original
      subject.embellished_excerpt
    end

    it 'uses default_embellishments from Configuration by... default' do
      Embellish.configure do |cfg|
        cfg.default_embellishments = [ExampleWrapperEmbellishment]
      end

      expect(Embellish::Embellisher).to receive(:new).with(subject.body, [ExampleWrapperEmbellishment])
                                                     .and_call_original
      subject.embellished_body
    end
  end
end
