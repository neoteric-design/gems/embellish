$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "embellish/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "embellish"
  s.version     = Embellish::VERSION
  s.authors     = ["Madeline Cowie"]
  s.email       = ["madeline@cowie.me"]
  s.homepage    = "https://www.neotericdesign.com"
  s.summary     = "Edit and transform rich markup"
  s.description = "Embellish transforms simplified markup into rich display HTML"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.2.0"
  s.add_dependency "nokogiri"
  s.add_dependency "ruby-oembed", '>= 0.12.0'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'rspec-rails', '>= 3.7.0'

  s.add_development_dependency 'factory_bot_rails', '>= 4.10.0'
  s.add_development_dependency 'simplecov'
end
