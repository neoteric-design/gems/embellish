require 'oembed'

module Embellish
  module Embellishments
    # OEmbed block support. Uses ruby-oembed, providers available depend on your
    # configuration.
    class Oembed < Base
      def target_nodes
        find_nodes('oembed')
      end

      def fetch_from_provider(src)
        ::OEmbed::Providers.get(src)
      end

      def transform_node(source_node)
        embed_code = begin
          provider = fetch_from_provider(source_node[:url])
          provider.html
        rescue OEmbed::NotFound
          ""
        end

        source_node.replace(embed_code)
      end
    end
  end
end
