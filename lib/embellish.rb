# fronzen_string_literal: true

require "embellish/railtie"
require "embellish/configuration"

module Embellish
  extend ActiveSupport::Autoload

  autoload :Embellisher
  autoload :Embellishments
  autoload :ActsAsEmbellished

  class << self
    def embellish(input, *embellishments)
      Embellisher.new(input, *embellishments).embellish
    end
  end
end
