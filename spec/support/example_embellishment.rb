class ExampleEmbellishment < Embellish::Embellishments::Base
  def target_nodes
    find_nodes('custom')
  end

  def transform_node(source_node)
    new_node = duplicate_node(source_node, new_type: 'div')
    assign_node_attributes(new_node, class: 'custom')

    source_node.replace(new_node)
  end
end

class ExampleWrapperEmbellishment < Embellish::Embellishments::Base
  def target_nodes
    find_nodes('img.custom')
  end

  def transform
    target_nodes.wrap('<figure></figure>')
  end
end
