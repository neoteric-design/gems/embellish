require 'rails_helper'

RSpec.describe Embellish::Embellisher do
  describe '::new' do
    it 'accepts embellishments as symbols' do
      subject = Embellish::Embellisher.new('', :oembed)

      expect(subject.embellishments).to eq([Embellish::Embellishments::Oembed])
    end

    it 'accepts embellishments as classes' do
      subject = Embellish::Embellisher.new('', :oembed, ExampleEmbellishment)

      expect(subject.embellishments).to eq([Embellish::Embellishments::Oembed,
                                            ExampleEmbellishment])
    end
  end
end
