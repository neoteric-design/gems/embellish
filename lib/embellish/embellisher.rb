module Embellish
  class Embellisher
    attr_reader :input, :embellishments

    def initialize(input, *embellishments)
      @input = input
      @embellishments = Array(embellishments).flatten.map do |embellishment|
        case embellishment
        when Class
          embellishment
        else
          "Embellish::Embellishments::#{embellishment.to_s.classify}"
            .constantize
        end
      end
    end

    def document
      @document ||= Nokogiri::HTML.fragment(input)
    end

    def embellish
      embellishments.each do |embellishment|
        embellishment.new(document).transform
      end

      document
    end
  end
end
