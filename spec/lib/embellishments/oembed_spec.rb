require 'rails_helper'
def build_container(link)
 Nokogiri::HTML.fragment("<oembed src='#{link}'></oembed>")
end

RSpec.describe Embellish::Embellishments::Oembed do
  let(:media_link) { 'http://www.youtube.com/watch?v=2BYXBC8WQ5k' }
  let(:document) { build_container(media_link)}
  let(:subject) { described_class.new(document) }

  before(:all) { OEmbed::Providers.register_all }

  it 'passes oembed containers to ruby-oembed' do
    expected = OEmbed::Providers.get(media_link).html

    subject.transform

    expect(document.to_s).to include(expected)
  end
end
