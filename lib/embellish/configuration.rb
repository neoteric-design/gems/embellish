# fronzen_string_literal: true

module Embellish
  class << self

    def config
      @config ||= Configuration.new
    end

    # Set configuration options
    #
    #   Embellish.configure do |config|
    #   end
    def configure
      @config = Configuration.new
      yield config
    end
  end

  # Gem config storage
  class Configuration
    SETTINGS = %i[default_embellishments].freeze
    attr_accessor(*SETTINGS)

    def initialize
      @default_embellishments = [:oembed]
    end
  end
end
