module Embellish
  module ActsAsEmbellished
    extend ActiveSupport::Concern

    included do
    end

    class_methods do
      def embellish_field(field, options = {})
        unless respond_to?(:embellished_fields)
          class_attribute :embellished_fields, default: {}
        end

        embellished_fields[field] = options.symbolize_keys

        define_method "embellished_#{field}" do
          embellishments = embellished_fields[field][:embellishments] ||
                           Embellish.config.default_embellishments
          Embellish.embellish(self.send(field), embellishments)
        end
      end
    end
  end
end
