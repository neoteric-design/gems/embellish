require 'rails_helper'

RSpec.describe Embellish do
  describe '::embellish' do
    it 'transforms a given special element' do
      input = "<custom></custom>"
      expected = "<div class=\"custom\"></div>"
      expect(Embellish.embellish(input, ExampleEmbellishment).to_s).to eq(expected)
    end
  end
end
