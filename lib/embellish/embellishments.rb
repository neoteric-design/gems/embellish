module Embellish
  module Embellishments
    extend ActiveSupport::Autoload

    autoload :Base
    autoload :Oembed
  end
end
