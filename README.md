# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Embellish

Transform simplified markup into rich display HTML

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'embellish'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install embellish
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
