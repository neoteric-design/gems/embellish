module Embellish
  module Embellishments
    class Base
      attr_reader :document

      def initialize(document)
        @document = document
      end

      def transform
        target_nodes.each do |target_node|
          transform_node(target_node)
        end
      end

      def target_nodes
        raise NotImplementedError
      end

      def build_node(tag)
        Nokogiri::XML::Node.new(tag, document)
      end

      def find_nodes(xpath)
        document.css(xpath)
      end

      def transform_node(source_node)
        raise NotImplementedError
      end

      def duplicate_node(source_node, new_type: nil)
        new_type ||= source_node.type

        node = build_node(new_type)
        node.content = source_node.content
        assign_node_attributes(source_node.attributes)

        node
      end

      def assign_node_attributes(node, **attributes)
        attributes.each do |key, value|
          node[key] = value
        end
      end
    end
  end
end
